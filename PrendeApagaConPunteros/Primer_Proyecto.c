#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void led2Init (void); // Set GPIO - P0_22 - to be output

#define LED2 22
#define ENT 23
#define AddrFIO0SET 0x2009C018
#define AddrFIO0CLR 0x2009C01C
#define AddrFIO0DIR 0x2009C000
#define AddrFIO0PIN 0x2009C014
#define FIO0SET ((unsigned int volatile*) AddrFIO0SET)
#define FIO0CLR ((unsigned int volatile*) AddrFIO0CLR)
#define FIO0DIR ((unsigned int volatile*) AddrFIO0DIR)
#define FIO0PIN ((unsigned int volatile*) AddrFIO0PIN)

/*unsigned int  *AddrFIO0SET;
unsigned int  *AddrFIO0CLR;
unsigned int  *AddrFIO0DIR;
unsigned int  *AddrFIO0PIN;*/

int main(void) {

	/*AddrFIO0SET = (unsigned int*) 0x2009C018;
	AddrFIO0CLR = (unsigned int*) 0x2009C01C;
	AddrFIO0PIN = (unsigned int*) 0x2009C014;
	unsigned int  *FIO0SET = (unsigned int*) AddrFIO0SET;
	unsigned int  *FIO0CLR = (unsigned int*) AddrFIO0CLR;*/

	led2Init();

	if(1 & *FIO0PIN){
		int i = 0 ;
		while(1) {
			for(i=10000000;i>0;i--)
			{
			}
			*FIO0SET = (1 << LED2);	//se enciende el led
			for(i=10000000;i>0;i--)
			{
			}
			*FIO0CLR = (1 << LED2);	//se apaga el led
			i++ ;
		}
	}
	return 0 ;
}

void led2Init (void) // Set GPIO - P0_22 - to be output and P0_23 to be input
{
	/*AddrFIO0DIR = (unsigned int*)0x02009C000;
	unsigned int * FIO0DIR = (unsigned int*) AddrFIO0DIR;*/
	*FIO0DIR |= (1 << LED2);
}
